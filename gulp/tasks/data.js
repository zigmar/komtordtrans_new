module.exports = function () {
	$.gulp.task('data', () => {
		return $.gulp.src('./dev/data/*.json')
			.pipe($.gulp.dest('./build/data'))
			.on('end', $.browserSync.reload);
	});
};
