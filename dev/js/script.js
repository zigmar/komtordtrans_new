$(document).ready(function(){
    let width = $(window).width();

    changeMobMenu();

    // burger 
    $('body').on('click', '.header-bottom .btm-burger', chooseBurger);

    if(width <= 1100){
        $('body').on('click', '.ct-title .btm-burger', chooseBurgerSect);
    }
    
    $(window).resize(changeMobMenu);


    // sliders
    $('.main-catalog-slider .slider-box').slick({
        cssEase: 'ease-in-out',
        slidesToShow: $('.slider-box').data('slide') || 4,
        slidesToScroll: 1,
        prevArrow: $(".arrows .arrow-prev"),
        nextArrow: $(".arrows .arrow-next"),
        infinite: true,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 920,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
        ]
    });

    $('.detail-card .slider-box').slick({
        cssEase: 'ease-in-out',
        slidesToShow: $('.slider-box').data('slide') || 4,
        slidesToScroll: 1,
        prevArrow: $(".button .left-arr"),
        nextArrow: $(".button .right-arr"),
        infinite: true,
        responsive: [
            {
                breakpoint: 1500,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 1280,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 850,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
        ]
    });

    $('.slide_line').slick({
        cssEase: 'ease-in-out',
        arrows: false,
        dots: true,
        fade: true,
        appendDots: $('.control__dots'),
        autoplay: true,
        autoplaySpeed: 5000,
        asNavFor: '.main-slider__box'
    });

    $('.main-slider__box').slick({
        cssEase: 'ease-in-out',
        arrows: false,
        dots: true,
        autoplay: true,
        autoplaySpeed: 5000,
    });

    $('.card-img-slider .card-slider-box').slick({
        cssEase: 'ease-in-out',
        slidesToShow: 1,
        arrows: false,
        slidesToScroll: 1,
        infinite: true,
    });



    if ($('.styler').length) {
        $('.styler').styler();
    }

    //меню в каталоге
    $('body').on('click', '.ct-sect > li.drop .list-sect .arrow', openDropdown);


    $('.direction-list ul').mCustomScrollbar({
        axis:"y",
        theme: 'dark'
    });

    $('.page__sidebar ul.ct-sect.autoparts').mCustomScrollbar({
        axis: "y",
        theme: 'dark'
    });

    $('.ajax-modal-video').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,

		fixedContentPos: false
    });
    
    $('body').on('click', '.card-img-list a.list-item', changeCardSlider);

    $('body').on('click', '.direction-list ul[data-role=list] li', changeDirection);

    $('a[data-fancybox="gallery"]').fancybox({
        padding: 0,
        margin: 5,
        nextEffect: 'fade',
        prevEffect: 'none',
        autoCenter: false,
        afterShow: function() {
            console.log('open');
        }
    });

    initMapsDelayScroll()
});


function changeDirection(e) {
    $(this).siblings().removeClass('active');
    $(this).addClass('active');
    var itemId = $(this).data('item');
    var itemDescription = $(this).closest('.our-direction').find('.direction-desc .tab[data-tab="'+itemId+'"]');
  
    itemDescription.siblings().removeClass('active');
    itemDescription.addClass('active');
}
  
function changeCardSlider(e) {
    e.preventDefault();

    var ref = $(this).data('ref');
    var indexSlider = $('.card-slider-box').find('.img-item[data-id="'+ref+'"]').attr('data-slick-index');

    $('.card-img-slider .card-slider-box').slick('slickGoTo', indexSlider);
}

// добавление на страницу отложенной загрузки внешнего скрипта
function loadDelayScript(url) {
    if (!window.loadedDelayScripts) {
        window.loadedDelayScripts = []
    }
    console.log(window.loadedDelayScripts)
    if (!~window.loadedDelayScripts.indexOf(url)) {
        window.loadedDelayScripts.push(url)
        var script = $(document.createElement('script'))
        script.attr({src: url, type: 'text/javascript'})
        $('body').append(script)
    }
}

// находится ли блок в видимой области вьюпорта или приближается к ней
function getVisibleElemOnScroll(elemJQ, reserveHeight) {
    if (elemJQ.length > 0) {
        return elemJQ.offset().top - ($(document).scrollTop() + $(window).height() + reserveHeight) < 0
    }
    return false
}

// отложенная инициализация яндекс карт по скроллу
function initMapsDelayScroll() {
    var win = $(window)
    var maps = $('.js-map')
    // для защиты от повторной инициализации
    var checkInitedMaps = false

    if (maps.length) {
        function initMaps() {
            // инициализируем если карта еще не инициализировалась
            if (!checkInitedMaps) {
                loadDelayScript('https://api-maps.yandex.ru/2.1.73/?lang=ru_RU&onLoad=initMaps')
                checkInitedMaps = true
            }
        }

        // запус инициализации если карта в зоне видимости или приближается к ней
        win.off('scroll.initMapsScroll').on('scroll.initMapsScroll', function () {
            if (getVisibleElemOnScroll(maps, 500)) initMaps()
        })
        win.trigger('scroll.initMapsScroll')
    }
}

// инициализация всех яндекс карт на сайте
function initMaps() {
    ymaps.ready(function () {
        // множество дефолтных меток
        $('#ymaps-multi-default').each(function () {
            console.log('iiiiiiiiiii')
            var mapItem = $(this)
            // путь к JSON файлу с данными для карты
            var pathJsonMulti = mapItem.data('json-path')

            var mapObject = new ymaps.Map(mapItem[0], {
                center: [125.79, 17.64],
                controls: ['geolocationControl', 'fullscreenControl'],
                zoom: 1,
            }, {
                searchControlProvider: 'yandex#search'
            })
            var objectManager = new ymaps.ObjectManager({
                // Чтобы метки начали кластеризоваться, выставляем опцию.
                clusterize: true,
                // ObjectManager принимает те же опции, что и кластеризатор.
                gridSize: 32,
                clusterDisableClickZoom: true
            })

            // Чтобы задать опции одиночным объектам и кластерам,
            // обратимся к дочерним коллекциям ObjectManager.
            objectManager.objects.options.set('preset', 'islands#blueDotIcon');
            objectManager.clusters.options.set('preset', 'islands#blueClusterIcons');

            // Загружаем GeoJSON файл с описанием объектов.
            $.getJSON(pathJsonMulti)
                .done(function (response) {
                    // Добавляем описание объектов в формате JSON в менеджер объектов.
                    objectManager.add(response);
                    // Добавляем объекты на карту.
                    mapObject.geoObjects.add(objectManager);

                    // устанавливаем нужный центр и зум карты, чтобы было видно все метки и задаем нужный отступ меток от края карты
                    mapObject.setBounds(mapObject.geoObjects.getBounds(), {checkZoomRange: true, zoomMargin: 50})
                        .then(function () {
                            //после установки проверяем зум и не даем выйти за рамки нужного
                            if (mapObject.getZoom() > 15) mapObject.setZoom(15);
                        });
                })
                .fail(function (response) {
                    console.warn('offer: ajax load failed', response)
                })
        })
    })
}

function chooseBurger(e) {
    $(this).toggleClass('active');
    $('.mob-menu').toggleClass('open');
    $('.mob-menu').slideToggle( "slow" );

}

function chooseBurgerSect(e) {
    $(this).toggleClass('active');
    $('.page__sidebar .ct-sect').slideToggle( "slow" );
}

function changeMobMenu(e = null) {
    let width = $(window).width();

    if(width <= 1000){
        $('.mob-menu__wrapper').append($('.btm-menu__top'));
        $('.mob-menu__wrapper').append($('.btm-menu__bottom'));
        $('.mob-menu__wrapper').prepend($('.header-top'));
    } else {
        $('.btm-menu').append($('.btm-menu__top'));
        $('.btm-menu').append($('.btm-menu__bottom'));
        $('.header .container').prepend($('.header-top'));
    }
}

function openDropdown(e) {
    e.preventDefault();
    console.log(e);
    const curElem = e.target;
    // if(!$(curElem).hasClass('list-sect')) return false;
    $(this).closest('.drop').toggleClass('open');
    $(this).closest('.drop').find('.dropdown').slideToggle( "slow" );
}